<?php
namespace AppBundle\Tests\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Document\Website;


/**
 * @MongoDB\Document(collection="websites",repositoryClass="AppBundle\Repository\WebsiteRepository")
 *
 * Generate set/get:
 * $ php app/console doctrine:mongodb:generate:documents AppBundle
 *
 * Timestamble:
 * https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/timestampable.md
 */
class WebsiteTest extends WebTestCase {

    private $em;
    private $container;
    private $repository;


    protected function setUp() {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine_mongodb')->getManager();


        $this->container = self::$kernel->getContainer();

        $this->repository = $this->container->get('doctrine_mongodb')
            ->getRepository('AppBundle:Website');
    }

    public function testPost() {
        $nameWebsite = 'http://www.sapo.pt';
        $website = new Website();
        $website->setName($nameWebsite);
        $this->em->persist($website);
        $this->em->flush();


        $this->assertGreaterThanOrEqual(1, $website->getId());
        $this->assertInternalType("string", $website->getId());
        $this->assertInstanceOf('AppBundle\Document\Website', $website);
    }

    public function testPut() {
        //Create
        $nameWebsite = 'http://www.google.pt';
        $website = new Website();
        $website->setName($nameWebsite);
        $this->em->persist($website);
        $this->em->flush();

        $idWebsite = $website->getId();

        //Update
        $websiteUpdated = $this->repository->find($idWebsite);
        $websiteUpdated->setName('http://www.google.pt/v2');
        $this->em->flush();

        $this->assertEquals($idWebsite, $websiteUpdated->getId());
        $this->assertInternalType("string", $websiteUpdated->getId());
        $this->assertInstanceOf('AppBundle\Document\Website', $websiteUpdated);
    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown() {
        parent::tearDown();
        $this->em->close();
    }
}
