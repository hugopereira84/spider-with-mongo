<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//file 
use Symfony\Component\Finder\Finder;

//document mongodb
use AppBundle\Document\Website;

class InitScavengerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('initScavenger')
            ->setDescription('Start Scavenger Websites');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        //start couting time
        $starttime = microtime();

        // connect to mongo to get list of websites to Scavenger
        $container = $this->getApplication()->getKernel()->getContainer();
        $repository = $container->get('doctrine_mongodb')
            ->getManager()
            ->getRepository('AppBundle:Website');
        $websites = $repository->findByLimitOffset(8);
         
        $output->writeln('Started adding list websites.');
        $output->writeln('-------------------------------');
        foreach($websites as $website){
            $idWebsite = $website->getId();
            $linkWebsite = $website->getName();
            $message = json_encode(['id'=>$idWebsite, "link"=>$linkWebsite]);
            $this->getContainer()->get('rabbitMQ_publish_website')->process($message);
            $output->writeln('Add website: "'.$linkWebsite.'" to rabbitmq.');
        }
       
        $output->writeln('-------------------------------');
        
        $duration = microtime() - $starttime;
        $output->writeln('It took:'.date("H:i:s", $duration));

        $output->writeln('Command result.');
    }

}
