<?php
 
namespace AppBundle\Service;
 
//Screen scraping and web crawling library for PHP
use Goutte\Client;
use AppBundle\Service\Symfony\Component\DomCrawler\Crawler as DomCrawler;
 
class Crawler 
{
    private $base_url;
    private $site_links;
    private $max_depth;
    private $container;
    

    public function __construct($max_depth, $container) {
        $this->max_depth = $max_depth;
        $this->container = $container;
    }


    public function init($base_url){
        // http protocol not included, prepend it to the base url
        if (!preg_match("~^(?:f|ht)tps?://~i", $base_url)) { 
            $base_url = 'http://' . $base_url;
        }
        $this->base_url = $base_url;
        $this->site_links = array();
    }
    
    /**
     * checks the uri if can be crawled or not
     * in order to prevent links like "javascript:void(0)" or "#something" from being crawled again
     * @param string $uri
     * @return boolean
     */
    protected function checkIfCrawlable($uri) {
        if (empty($uri)) {
            return false;
        }

        //returned deadlinks
        $stop_links = array(
            '@^javascript\:void\(0\)$@',
            '@^#.*@',
        );

        foreach ($stop_links as $ptrn) {
            if (preg_match($ptrn, $uri)) {
                return false;
            }
        }

        return true;
    }

    /**
     * normalize link before visiting it
     * currently just remove url hash from the string
     * @param string $uri
     * @return string
     */
    protected function normalizeLink($uri) {
        $uri = preg_replace('@#.*$@', '', $uri);

        return $uri;
    }

    /**
     * initiate the crawling mechanism on all links
     * @param string $url_to_traverse
     */
    public function traverse($url_to_traverse = null) {
        if (is_null($url_to_traverse)) {
            $url_to_traverse = $this->base_url;

            //initialize first element in the site_links 
            $this->site_links[$url_to_traverse] = array(
                'links_text' => array("BASE_URL"),
                'absolute_url' => $url_to_traverse,
                'frequency' => 1,
                'visited' => false,
                'external_link' => false,
                'original_urls' => array($url_to_traverse),
            );
        }

        $this->_traverseSingle($url_to_traverse, $this->max_depth);
    }

    /**
     * crawling single url after checking the depth value
     * @param string $url_to_traverse
     * @param int $depth
     */
    protected function _traverseSingle($url_to_traverse, $depth) {
        try {
            echo PHP_EOL.PHP_EOL."-> Link to traverse:".$url_to_traverse.PHP_EOL;
            
            $client = new Client();
            $crawler = $client->request('GET', 
                        $url_to_traverse, 
                        [   
                            'verify' => false
                        ]);
            $clientResponse = $client->getResponse();
           
            $statusCode = $clientResponse->getStatus();
            $this->site_links[$url_to_traverse]['status_code'] = $statusCode;
            
            $headersPage = $clientResponse->getHeaders();
            $this->site_links[$url_to_traverse]['headers_page'] = $headersPage;
            
            
            if ($statusCode == 200) { // valid url and not reached depth limit yet            
                $content_type = $clientResponse->getHeader('Content-Type');  
                
                //traverse children in case the response in HTML document 
                if (strpos($content_type, 'text/html') !== false) { 
                    //information needed to validate technologies
                    $this->extractCssFilesInfo($crawler, $url_to_traverse);
                    $this->extractJsInfo($crawler, $url_to_traverse);
                    $this->extractHtmlInfo($crawler, $url_to_traverse);
                    
                    $this->getTechnologieUsed($url_to_traverse);
                   

                    $current_links = [];

                    // for internal uris, get all links inside
                    if (
                     isset($this->site_links[$url_to_traverse]['external_link']) 
                     && 
                     $this->site_links[$url_to_traverse]['external_link'] == false) { 
                       $current_links = $this->extractLinksInfo($crawler, $url_to_traverse);
                    }

                    // mark current url as visited
                    $this->site_links[$url_to_traverse]['visited'] = true; 

                    $this->traverseChildLinks($current_links, $depth - 1);
                }
            }
            
        } catch (\GuzzleHttp\Exception\ConnectException $ex) {
            error_log("Could not resolve host: " . $url_to_traverse);
            $this->site_links[$url_to_traverse]['status_code'] = '404';
        } catch (\Exception $ex) {
            error_log("<error>-> Error</error> retrieving data from link: " . $url_to_traverse.", error:".$ex->getMessage());
            $this->site_links[$url_to_traverse]['status_code'] = '404';
        }
    }

    /**
     * after checking the depth limit of the links array passed
     * check if the link if the link is not visited/traversed yet, in order to traverse
     * @param array $current_links
     * @param int $depth     
     */
    protected function traverseChildLinks($current_links, $depth) {
        if ($depth == 0) {
            return;
        }

        foreach ($current_links as $uri => $info) {
            if (!isset($this->site_links[$uri])) {
                $this->site_links[$uri] = $info;
            } else{
                $original_urls = isset($this->site_links[$uri]['original_urls'])
                        ? array_merge($this->site_links[$uri]['original_urls'], 
                                $info['original_urls'])
                        :   $info['original_urls'];
                
                $links_text = isset($this->site_links[$uri]['links_text'])
                        ? array_merge($this->site_links[$uri]['links_text'], 
                                $info['links_text'])
                        :   $info['links_text'];
                
                $this->site_links[$uri]['original_urls'] = $original_urls;
                $this->site_links[$uri]['links_text'] = $links_text;
                
                //already visited link
                if($this->site_links[$uri]['visited']) { 
                    $infoFrequency = isset($info['frequency']) ? 
                                    $info['frequency'] : 0;
                    $uriFrequency = isset($this->site_links[$uri]['frequency']) ? 
                                    $this->site_links[$uri]['frequency'] : 0;
                    
                    $this->site_links[$uri]['frequency'] = 
                            $uriFrequency + 
                            $infoFrequency;
                }
            }

            if (!empty($uri) && 
                !$this->site_links[$uri]['visited'] && 
                !isset($this->site_links[$uri]['dont_visit'])
                ) { 
                    //traverse those that not visited yet   
                    $normalizeLink = $this->normalizeLink(
                            $current_links[$uri]['absolute_url']);
                    $this->_traverseSingle($normalizeLink, $depth);
            }
        }
    }

    /**
     * extracting all <a> tags in the crawled document, 
     * and return an array containing information about links like: uri, absolute_url, frequency in document
     * @param Symfony\Component\DomCrawler\Crawler $crawler
     * @param string $url_to_traverse
     * @return array
     */
    protected function extractLinksInfo(&$crawler, $url_to_traverse) {
        $current_links = array();
        $crawler->filter('a')->each(function($node, $i) use (&$current_links) {
            $node_text = trim($node->text());
            $node_url = $node->attr('href');
            $hash = $this->normalizeLink($node_url);

            if (!isset($this->site_links[$hash])) {  
                $current_links[$hash]['original_urls'][$node_url] = $node_url;
                $current_links[$hash]['links_text'][$node_text] = $node_text;

                if (!$this->checkIfCrawlable($node_url)){

                }elseif (!preg_match("@^http(s)?@", $node_url)) { //not absolute link                            
                    $current_links[$hash]['absolute_url'] = $this->base_url . $node_url;
                } else {
                    $current_links[$hash]['absolute_url'] = $node_url;
                }

                if (!$this->checkIfCrawlable($node_url)) {
                    $current_links[$hash]['dont_visit'] = true;
                    $current_links[$hash]['external_link'] = false;
                } elseif ($this->checkIfExternal($current_links[$hash]['absolute_url'])) { // mark external url as marked                            
                    $current_links[$hash]['external_link'] = true;
                } else {
                    $current_links[$hash]['external_link'] = false;
                }
                
                $current_links[$hash]['visited'] = false;
                $current_links[$hash]['frequency'] = isset($current_links[$hash]['frequency']) ? $current_links[$hash]['frequency']++ : 1; // increase the counter
            }
                    
        });

        if (isset($current_links[$url_to_traverse])) { // if page is linked to itself, ex. homepage
            $current_links[$url_to_traverse]['visited'] = true; // avoid cyclic loop                
        }
        return $current_links;
    }
    
    
    /**
     * extract all html information
     * @param object $crawler
     * @param string $uri
     */
    protected function extractHtmlInfo(&$crawler, $url) {
        $html = '';
        foreach ($crawler as $domElement) {
            $html .= $domElement->ownerDocument->saveHTML($domElement);
        }
        
        $this->site_links[$url]['html'] = $html;
    }
    /**
     * extract information about document title
     * @param object $crawler
     * @param string $uri
     */
    protected function extractTitleInfo(&$crawler, $url) {
        echo '-> Extract title info, from:'.$url.PHP_EOL;
        
        $this->site_links[$url]['title'] = trim($crawler->filterXPath('//title')->text());
    }
    /**
     * extract all h1 tags in page
     * @param object $crawler
     * @param string $uri
     */
    protected function extractH1Info(&$crawler, $url) {
        echo '-> Extract h1 info, from:'.$url.PHP_EOL;
        
        $h1_count = $crawler->filter('h1')->count();
        $this->site_links[$url]['h1_count'] = $h1_count;
        $this->site_links[$url]['h1_contents'] = array();

        if ($h1_count) {
            $crawler->filter('h1')->each(function($node, $i) use($url) {
                        $this->site_links[$url]['h1_contents'][$i] = trim($node->text());
            });
        }
    }
    /**
     * extract all images sources  in page
     * @param object $crawler
     * @param string $uri
     */
    protected function extractImagesInfo(&$crawler, $url) {
        echo '-> Extract images sources info, from:'.$url.PHP_EOL;
        
        $imgSources = $crawler->filterXpath('//img')
                                ->extract(array('src'));
        $this->site_links[$url]['images'] = $imgSources;
    }
    protected function extractCssFilesInfo(&$crawler, $url) {
        echo '-> Extract css sources info, from:'.$url.PHP_EOL;
        
        $srcCss = [];
        $lstCss = $crawler->filterXPath('//link');
        foreach($lstCss as $info){
            $srcCss[] = $info->getAttribute('href');
        }
        
        $this->site_links[$url]['source_css'] = $srcCss;
    }
    protected function extractJsInfo(&$crawler, $url) {
        echo '-> Extract js sources info, from:'.$url.PHP_EOL;
        
        $srcJs = [];
        $lstJs = $crawler->filterXPath('//script');
        foreach($lstJs as $info){
            $srcJs[] = $info->ownerDocument->saveHTML($info);
        }
        
        $this->site_links[$url]['source_js'] = $srcJs;
    }
    
    
    protected function getTechnologieUsed($url_to_traverse){
        $crawlerHelper = $this->container->get('crawler_helper');
        $crawlerHelper->setDataPage($this->site_links[$url_to_traverse]);
        $technologiesUsed = $crawlerHelper->getTechnologies();


        $technologies = [];
        foreach($technologiesUsed as $key => $value){
            $technologies[key($value)] = $value[key($value)];
        }


        $this->site_links[$url_to_traverse]['technologies'] = $technologies;
    }
    

    
    

    /**
     * getting information about links crawled
     * @return array
     */
    public function getLinksInfo() {
        return $this->site_links;
    }

    /**
     * check if the link leads to external site or not
     * @param string $url
     * @return boolean
     */
    public function checkIfExternal($url) {
        $base_url_trimmed = str_replace(array('http://', 'https://'), '', $this->base_url);

        if (preg_match("@http(s)?\://$base_url_trimmed@", $url)) { //base url is not the first portion of the url
            return false;
        } else {
            return true;
        }
    }
}