<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//file 
use Symfony\Component\Finder\Finder;

//document mongodb
use AppBundle\Document\Website;

class InitDbFixturesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('initDbFixtures')
            ->setDescription('Create MongoDb and start fixtures')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        //start couting time
        $starttime = microtime();

        // connect to mongo to add list of websites to Scavenger
        $this->container = $this->getApplication()->getKernel()->getContainer();
        $m = $this->container->get('doctrine_mongodb.odm.default_connection');
        $nameDbMongo = $m->getConfiguration()->getDefaultDB();
        $db = $m->selectDatabase($nameDbMongo);
        $collWeb = $db->createCollection('websites');
        
        //get contents of websites list to add to mongo
        $finder = new Finder();
        $pathWebsitesLst = dirname(__DIR__). DIRECTORY_SEPARATOR ."DataFixtures". DIRECTORY_SEPARATOR ."MongoDB";
        $finder->files()->in($pathWebsitesLst)->name('websites.txt');
        
        $output->writeln('Started adding list websites.');
        $output->writeln('-------------------------------');
        $numberLines = 1;
        foreach ($finder as $file) {
            $file  = $file->openFile();
            foreach ($file as $lineNumber => $line) {
                $userLineNumber = $lineNumber + 1;
                $infoLine = trim(preg_replace('/\s\s+/', ' ', $line));
                
                if(empty($infoLine)){
                    continue;
                }
                
                $nameWebsite = '';
                $webSiteInfo = explode("|", $infoLine);
                foreach($webSiteInfo as $key => $info){
                    if($key == 0){
                        $nameWebsite = trim($info);
                    }
                }
                
                //add content to mongodb, trough document
                $website = new Website();
                $website->setName($nameWebsite);
                $dm = $this->container->get('doctrine_mongodb')->getManager();
                $dm->persist($website);
                $dm->flush();
                
                $websiteIdCreated = $website->getId();
                $state = isset($websiteIdCreated) ? 
                        '<info>sucess</info>' : '<error>failure</error>';
                $output->writeln('('.$userLineNumber.')'. 'Add website: "'.$nameWebsite.'" with "'.$state.'" to mongodb.');
                
            }
        }
        $output->writeln('-------------------------------');
        
        $duration = microtime() - $starttime;
        $output->writeln('It took:'.date("H:i:s", $duration));

        $output->writeln('Command result.');
    }

}
