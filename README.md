Scavenger
-------------------

Technologies used:
- Symfony 2
- MongoDB 2.6.9
- PHP 5.5.9
- RabbitMQ (help: http://goo.gl/TSjiQX)

----------------------------------------------------

Steps to use scanvager:

--

Install Db fixtures (migrations) / packages  ([**install**](INSTALL.md) )

--

This applications uses rabbitmq, so it can run multiple workers to process content of websites
( see the steps [**here**](RABBIT_MQ.md) to see the steps you need to do, to make it work)
 
--

To implement new detection of new technologies, you will need to implement a helper under (src/AppBundle/Helper/) 
with the name CrawlerDetectTechnologies.php. 
Just create a new function on that helper and return an array with the technology 
with a value of true/false, if exists or not. 
Parameters that can help: cssPage / jsPage / html.

--

To add/edit/remove new websites, you can go to the root of the website.

--

To Run tests:
$phpunit -c app src/AppBundle/Tests/Document/WebsiteTest.php