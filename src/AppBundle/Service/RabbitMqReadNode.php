<?php
 
namespace AppBundle\Service;
 
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

//get container
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RabbitMqReadNode implements ConsumerInterface
{
    private $logger;
    private $container;
    
    public function __construct(LoggerInterface $logger, Container $container)
    {
        $this->logger = $logger;
        $this->container = $container;
    }
    
    
 
    public function execute(AMQPMessage $msg)
    {
        //read message body
        $messageBody = json_decode($msg->body, true);

        //fetch content of message body website
        $idWebsite = $messageBody['id'];
        $linkWebsite = $messageBody['link'];

        //search on webpages of website content needed
        $crawlerService = $this->container->get('crawler');
        $crawlerService->init($linkWebsite);
        $crawlerService->traverse();    
        $dataWebsite = $crawlerService->getLinksInfo();

        //store information on mongodb
        $m = $this->container->get('doctrine_mongodb.odm.default_connection');
        $nameDbMongo = $m->getConfiguration()->getDefaultDB();
        $db = $m->selectDatabase($nameDbMongo);
        $collection = $db->createCollection('dataWebsites');

        // add a record
        $document = ["idWebsite" => $idWebsite, 
                    "nameWebsite"=> $linkWebsite,  
                    "dataWebsites" => $dataWebsite];
        $collection->insert($document);
        
        
        /*$client = new Client();
        $crawler = $client->request('GET', $linkWebsite);
        $status_code = $client->getResponse()->getStatus();
        echo "status:".$status_code;
        if($status_code==200){
            //process the documents
            $txtBodyHtml = $crawler->filter('body')->text();
            echo $txtBodyHtml;
        }else{
            echo 'vai aqui';
        }*/
        
        //if found, add a new collection to mongo "website_content"
        
        
        
        // $msg->body is a data sent by RabbitMQ, in our example it contains XML
        //$sxe = new \SimpleXMLElement($msg->body);
        
        // Now it's completely up to what you will do with this XML
        // You can do anything! But we will just log that we processed XML node
        
        //$this->logger->info(sprintf('Node processed: "%s"', $sxe->title));
    }
}