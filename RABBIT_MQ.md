Scavenger
-------------------

Technologies used:
- Symfony 2
- MongoDB 2.6.9
- PHP 5.5.9
- RabbitMQ (help: http://goo.gl/TSjiQX)

----------------------------------------------------

RABBIT MQ

Add list websites to rabbitmq, to scanvage websites:
$ php app/console initScavenger

Consume list to scanvage websites to see if there is content pretended (only 
run this line on terminal, if your not using supervisor):
$ php app/console rabbitmq:consumer -w web_crawler 