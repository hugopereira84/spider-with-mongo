Scavenger challenge
-------------------

Technologies used:
- Symfony 2
- MongoDB 2.6.9
- PHP 5.5.9
- RabbitMQ (help: http://goo.gl/TSjiQX)

----------------------------------------------------

To do: 
- Crud for websites collection (test routing and controller)
- phpunit tests
- Implement metadata_cache_driver with memcache
