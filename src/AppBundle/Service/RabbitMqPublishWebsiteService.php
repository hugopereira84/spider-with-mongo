<?php

namespace AppBundle\Service;

class RabbitMqPublishWebsiteService {
    private $producer;
    
    
    public function __construct($producer)
    {
        $this->producer = $producer;
    }

    public function process($linkWebsite)
    {
        $this->producer->publish($linkWebsite);
    }

}
