<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @MongoDB\Document(collection="websites",repositoryClass="AppBundle\Repository\WebsiteRepository")
 *
 * Generate set/get:
 * $ php app/console doctrine:mongodb:generate:documents AppBundle
 *
 * Timestamble:
 * https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/timestampable.md
 */
class Website
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var String
     *
     * @MongoDB\Field(type="string")
     */
    protected $name;

     /**
     * @var date $created
     *
     * @MongoDB\Date
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @var date $updated
     *
     * @MongoDB\Date
     * @Gedmo\Timestampable
     */
    private $updated;


    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return custom_id $id
     */
    public function getId()
    {
        return $this->id;
    }
}
