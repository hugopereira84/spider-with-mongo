Scavenger
-------------------

Technologies used:
- Symfony 2
- MongoDB 2.6.9
- PHP 5.5.9
- RabbitMQ (help: http://goo.gl/TSjiQX)

----------------------------------------------------

INSTALLING MIGRATIONS / NEEDED PACKAGES

--

Install Db fixtures (import websites list to mongodb): 
. Run symfony command (root of project): php app/console initDbFixtures

--

Install RabbitMq: 
First of all you will want to get all the various tools set up and we will 
begin by installing RabbitMQ. The following steps are the installation for my 
Ubuntu platform, but if you would like instructions for another system, 
there is good documentation available – pick your system from the list on the 
right hand side.

1. As we want to use newest version of RabbitMQ we will use their repository 
to fetch the packages from. To do this, add the following line 
to your /etc/apt/sources.list:
$ deb http://www.rabbitmq.com/debian/ testing main
 
2. To use the new repository, we will also need to add an entry to our 
trusted keys list, like this:
$ wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
$ sudo apt-key add rabbitmq-signing-key-public.asc

3. Now we can update our packages and then install RabbitMQ:
$ sudo apt-get update
$ sudo apt-get install rabbitmq-server

 
At this point, the installation should complete successfully and we should 
have RabbitMQ running on our local system. We can check everything is working 
as expected by running the status command:
$ sudo rabbitmqctl status

--

Install the RabbitMQ Admin Panel:
The admin panel for RabbitMQ is not required for today’s tutorial, 
but I highly recommend it; it is a web-based admin panel which simplifies 
management of RabbitMQ by allowing you to see statistics, see a list of queues, 
display the messages, add messages by hand and many other tricks.

To install the admin panel, we add it as a plugin to RabbitMQ by executing the 
following command:
$ sudo rabbitmq-plugins enable rabbitmq_management

 
Now to get an access to admin panel you just need to visit 
http://server-name:15672/, where server-name is the domain name for where 
RabbitMQ is installed, and 15672 is the default port on which the RabbitMQ, 
web server is listening. By default, the login and password for a new 
installation are “guest” and “guest” respectively.

-- 

Install supervisor (to have consumer running in background):
$ sudo apt-get install supervisor

To ensure the Supervisor is installed, you can run following command:
$ sudo service supervisor restart

To put the consumer working with the supervisor, run following commands (on root
of project folder):
$ php app/console rabbitmq-supervisor:control start 
$ php app/console rabbitmq-supervisor:rebuild

First line above is to start the bundle (always required), while second line 
is to rebuild reconfigurations in case rabbitmq configurations or the file 
of the consumer changes (optional).
 