<?php
 
namespace AppBundle\Helper;
 
//Screen scraping and web crawling library for PHP
use Goutte\Client;
use AppBundle\Service\Symfony\Component\DomCrawler\Crawler as DomCrawler;
 
class CrawlerDetectTechnologies
{
    private $data;
    private $blackListMethods = ['setDataPage','getTechnologies'];


    public function setDataPage($data){
        $this->data = $data;
    }

    public function getTechnologies(){
        $listTechnologies = [];

        $class = new \ReflectionClass(__CLASS__);
        $methods = $class->getMethods();
        $methodsToExecute = [];
        foreach($methods as $method){
            if(!in_array($method->name, $this->blackListMethods)){
                $listTechnologies[] = call_user_func(array(__CLASS__, $method->name));
            }
        }

        return $listTechnologies;
    }
    


    /**
     * Validate if website has Magento, eCommerce software and platform
     */
    protected function checkWebSiteHas_Magento(){
        echo "-> Check WebSite Has Magento";

        $result = [];

        $isUsingMagento = 0;
        $flagCookies = 0;
        $flagCssJs = 0;
        $cssPage = $this->data['source_css'];
        $jsPage = $this->data['source_js'];
        $rulesCssJs = array_merge($cssPage, $jsPage);


        $rules_Magento = [
            "cats"      => 6,
            "env"       => "^(?:Mage|VarienForm)$",
            "headers_cookies"   => [
                "/.*frontend=([0-9]|[a-z])*/",
                "/confidence:50/"
            ],
            "icon"      => "Magento.png",
            "implies"   => "PHP",
            "css_js"    => [
                "/js\/mage/",
                "/.*skin.*\/frontend\/(?:default|(enterprise)).*/"
            ],
            "website"   => "www.magentocommerce.com"
        ];

        //validate cookies
        if(isset($this->data
            ['headers_page']['Set-Cookie'])){
            $cookiesPage = $this->data
            ['headers_page']['Set-Cookie'];
            $cookiesHeaders = $rules_Magento['headers_cookies'];
            echo PHP_EOL."-> Check WebSite Has Magento: validate cookies";
            foreach($cookiesHeaders as $ruleCookie){
                foreach($cookiesPage as $cookie){
                    if (preg_match($ruleCookie, $cookie)) {
                        $flagCookies = 1;
                    }

                }
            }
        }


        //validate css/js
        echo PHP_EOL."-> Check WebSite Has Magento: validate css/js".PHP_EOL;
        foreach($rules_Magento['css_js'] as $rulecssjs){
            foreach($rulesCssJs as $cssJs){
                if (preg_match($rulecssjs, $cssJs)) {
                    $flagCssJs = 1;
                }
            }
        }

        if($flagCookies == 1 && $flagCssJs == 1){
            $isUsingMagento = 1;
        }

        $result['is_using_magento'] = $isUsingMagento;
        return $result;
    }
    /**
     * @todo: WIP - Validate if website has Shopify, ecommerce software
     */
    protected function checkWebSiteHas_Shopify(){
        echo "-> Check WebSite Has Shopify";

        $result = [];

        $isUsingShopify = 0;
        $flagCssJs = 0;
        $cssPage = $this->data['source_css'];
        $jsPage = $this->data['source_js'];
        $rulesCssJs = array_merge($cssPage, $jsPage);

        $rules_Shopify = [
            "cats"      => 6,
            "env"       => "^Shopify$",
            "css_js"    => ["/\/\/cdn.shopify.com.*/"],
            "icon"      => "Shopify.png",
            "website"   => "shopify.com"
        ];

        //validate css/js
        echo PHP_EOL."-> Check WebSite Has Shopify: validate css/js".PHP_EOL.PHP_EOL;
        foreach($rules_Shopify['css_js'] as $ruleCss){
            foreach($rulesCssJs as $cssJs){
                if (preg_match($ruleCss, $cssJs)) {
                    $flagCssJs = 1;
                }
            }
        }

        if($flagCssJs == 1){
            $isUsingShopify = 1;
        }

        $result['is_using_shopify'] = $isUsingShopify;
        return $result;
    }
    /**
     * Validate if website has BigCommerce, ecommerce platform
     */
    protected function checkWebSiteHas_BigCommerce(){
        echo "-> Check WebSite Has BigCommerce";

        $result = [];

        $isUsingBigCommerce = 0;
        $flagCssJs = 0;
        $cssPage = $this->data['source_css'];
        $jsPage = $this->data['source_js'];
        $rulesCssJs = array_merge($cssPage, $jsPage);

        $rules_Shopify = [
            "cats"      => 6,
            "env"       => "^compareProducts$",
            "css_js"    => ["/.*cdn\d+.bigcommerce.com.*/"],
            "icon"      => "Bigcommerce.png",
            "url"       => "mybigcommerce\\.com",
            "website"   => "www.bigcommerce.com"
        ];

        //validate css/js
        echo PHP_EOL."-> Check WebSite Has Shopify: validate css/js".PHP_EOL.PHP_EOL;
        foreach($rules_Shopify['css_js'] as $ruleCss){
            foreach($rulesCssJs as $cssJs){
                if (preg_match($ruleCss, $cssJs)) {
                    $flagCssJs = 1;
                }
            }
        }

        if($flagCssJs == 1){
            $isUsingBigCommerce = 1;
        }

        $result['is_using_bigcommerce'] = $isUsingBigCommerce;
        return $result;
    }
    /**
     * Validate if website has Zendesk, Customer service software and support ticket software
     */
    protected function checkWebSiteHas_Zendesk(){
        echo "-> Check WebSite Has Zendesk";

        $result = [];
        $isUsingZendesk = 0;

        $htmlPage = $this->data['html'];
        if (strpos(strtolower($htmlPage), 'zendesk') !== false) {
            $isUsingZendesk = 1;
        }

        $result['is_using_zendesk'] = $isUsingZendesk;
        return $result;
    }
    protected function checkWebSiteHas_Desk(){
        echo "-> Check WebSite Has Desk";

        $result = [];
        $isUsingDesk = 0;
        $htmlPage = $this->data['html'];
        if (strpos(strtolower($htmlPage), 'desk') !== false) {
            $isUsingDesk = 1;
        }

        $result['is_using_desk'] = $isUsingDesk;
        return $result;
    }
    /**
     * Validate if website has Zendesk, customer support software and helpdesk
     */
    protected function checkWebSiteHas_Freshdesk(){
        echo "-> Check WebSite Has Freshdesk";

        $result = [];
        $isUsingFreshdesk = 0;
        $htmlPage = $this->data['html'];
        if (strpos(strtolower($htmlPage), 'freshdesk') !== false) {
            $isUsingFreshdesk = 1;
        }

        $result['is_using_freshdesk'] = $isUsingFreshdesk;
        return $result;
    }

}